/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects;

/**
 *
 * @author skhumalo
 */
public class Inspection_Scheduler_PageObjects
{

    public static String InspectionScheduler_Module;

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String EvaluatePerformanceMaintenance()
    {
        return "//div[@original-title='Evaluate Performance Maintenance']//i[@class='icon settings size-five cool grey module iso-icon row']";
    }

    public static String InspectionScheduler_Module()
    {
        return "//label[text()='Inspections Scheduler']";
    }

    public static String InspectionScheduler_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String InspectionScheduler_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480']";
    }

    public static String InspectionSchedulerRecurrence_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
    }
    
    public static String InspectionScheduler_Dropdown()
    {
        return "//div[@id='control_BFE2139A-F39D-4F4E-BD6D-572B68546C05']";
    }
    
    public static String businessUnitDisabled_Dropdown()
    {
        return "(//div[@id='control_BFE2139A-F39D-4F4E-BD6D-572B68546C05']/div)[1]";
    }

    public static String BusinessUnit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String BusinessUnit_Option1(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String ImpactType_Dropdown()
    {
        return "//div[@id='control_8876D62C-BC85-4F02-A389-D78DC27E54BB']//li";
    }

    public static String ImpactType_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String CloseImpactType_Dropdown()
    {
        return "//div[@id='control_8876D62C-BC85-4F02-A389-D78DC27E54BB']//b[@class='select3-down drop_click']";
    }

    public static String InspectionType_Dropdown()
    {
        return "//div[@id='control_C7BC891B-0559-4895-B1D4-FFF3B498BE59']";
    }
    
     public static String ImpactTypeDisabled_Dropdown()
    {
        return "(//div[@id='control_8876D62C-BC85-4F02-A389-D78DC27E54BB']/div)[1]";
    }   


    public static String Project_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String ReplicateInspectionTo_Dropdown()
    {
        return "//div[@id='control_D9058671-DFF1-4B10-AC86-D18E77DC95B2']";
    }

    public static String ApplicableUsers_Option(String data)
    {
        return "//div[@id='control_8777C05F-0A7B-4735-B937-19DC86E69BFF']//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String SaveButton()
    {
        return "//div[@id='btnSave_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String InspectionType_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String ApplicableGroups_Option(String data)
    {
        return "//div[@id='control_B912FBD3-878C-45BD-B9A9-ACA0D45A9383']//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String InspectionSchedulerRecurrence_Panel()
    {
        return "//span[text()='Inspection Scheduler Recurrence']";
    }

    public static String Recurrence_Add()
    {
        return "//div[@id='control_1FC82892-73AC-41B3-A7F5-2D8CC21EA42F']//div[text()='Add']";
    }

    public static String Recurrence_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
    }

    public static String StartDate()
    {
        return "//div[@id='control_0F04A24A-49B8-4229-84E1-3816AF826DD7']//input[@type='text']";
    }

    public static String RecurrenceFrequency_Dropdown()
    {
        return "//div[@id='control_1FD95F7C-2974-4DFD-BAE6-8693FA639CFA']";
    }

    public static String RecurrenceFrequency_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String RecurrenceFrequency_CheckBox()
    {
         return "//div[@id='control_368F387B-E638-4592-A429-7397542FE787']//div[@class='c-chk']";
    }
    public static String RecurrenceFrequency_SaveButton()
    {
        return "//div[@id='btnSave_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
    }

    public static String NumberOfDays()
    {
        return "//div[@id='control_0F94299B-F336-4626-9839-554CD71BF769']//input[@type='number']";
    }

    public static String EndDate()
    {
        return "//div[@id='control_DCC55A98-9EAA-452B-9823-783816390399']//input[@type='text']";
    }

    public static String DayOfTheWeek_Dropdown()
    {
        return "//div[@id='control_38888542-B6ED-4900-9C0F-E6BB11DFB754']";
    }

    public static String WeekOfMonth_Dropdown()
    {
        return "//div[@id='control_69B70CD4-110F-4DFB-ADA9-C3E974F7728B']";
    }

    public static String AnnuallyFrom_Dropdown()
    {
        return "//div[@id='control_0AD5C3DC-29C4-4F21-B8C6-E008B836B316']";
    }

    public static String Inspection_Scheduler_Recurrence_Heading()
    {
        return "//div[text()='Inspection Scheduler Recurrence']";

    }
    public static String Inspection_Scheduler_Recurrence_Label()
    {
        return "//div[@class='c-pnl']//i";

    }

    public static String Inspection_Scheduler_Recurrence_Add_Button()
    {
        return "//div[@id='btnAddNew']//div[text()='Add']";
    }

    public static String RecurrenceFrequency_Dropdown_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";

    }

}
