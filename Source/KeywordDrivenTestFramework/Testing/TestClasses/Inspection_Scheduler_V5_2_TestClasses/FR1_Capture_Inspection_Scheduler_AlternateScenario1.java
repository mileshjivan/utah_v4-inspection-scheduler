/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Inspection Scheduler - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR1_Capture_Inspection_Scheduler_AlternateScenario1 extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Inspection_Scheduler_AlternateScenario1()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Navigate_To_Inspection_Scheduler()
    {
        //Environmental Health and Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.navigate_EHS()))
        {
            error = "Failed to wait for 'Environmental Health and Safety' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.navigate_EHS()))
        {
            error = "Failed to click on 'Environmental Health and Safety' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental Health and Safety' module.");

        //Evaluate Performance Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EvaluatePerformanceMaintenance()))
        {
            error = "Failed to wait for 'Evaluate Performance Maintenance' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.EvaluatePerformanceMaintenance()))
        {
            error = "Failed to click on 'Evaluate Performance Maintenance' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Evaluate Performance Maintenance' module.");

        //Inspection Scheduler
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspection Scheduler' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Inspection_Scheduler()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Dropdown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Dropdown()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.BusinessUnit_Option1(getData("Business Unit 2"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.BusinessUnit_Option(getData("Business Unit 3"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ImpactType_Dropdown()))
        {
            error = "Failed to wait for Impact type dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ImpactType_Dropdown()))
        {
            error = "Failed to click the Impact type dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Impact type dropdown.");

        //Impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ImpactType_Option(getData("Impact type"))))
        {
            error = "Failed to wait for Impact type option: " + getData("Impact type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ImpactType_Option(getData("Impact type"))))
        {
            error = "Failed to click the Impact type option: " + getData("Impact type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Impact type option: " + getData("Impact type"));

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.CloseImpactType_Dropdown()))
        {
            error = "Failed to click the Close Impact type dropdown.";
            return false;
        }

        //Inspection type
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionType_Dropdown()))
        {
            error = "Failed to wait for Inspection type dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionType_Dropdown()))
        {
            error = "Failed to click the Inspection type dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Inspection type dropdown.");

        //Inspection type select
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionType_Option(getData("Inspection type"))))
        {
            error = "Failed to wait for Inspection type option: " + getData("Inspection type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionType_Option(getData("Inspection type"))))
        {
            error = "Failed to click the Inspection type option: " + getData("Inspection type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Inspection type option: " + getData("Inspection type"));

        //Replicate inspections to
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ReplicateInspectionTo_Dropdown()))
        {
            error = "Failed to wait for Replicate inspections to dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ReplicateInspectionTo_Dropdown()))
        {
            error = "Failed to click the Replicate inspections to dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Replicate inspections to dropdown.");

        //Replicate inspections to select
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Project_Option(getData("Replicate inspections to"))))
        {
            error = "Failed to wait for Replicate inspections to option: " + getData("Replicate inspections to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Project_Option(getData("Replicate inspections to"))))
        {
            error = "Failed to click the Replicate inspections to option: " + getData("Replicate inspections to");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Replicate inspections to option: " + getData("Replicate inspections to"));

        //Select the applicable groups
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ApplicableGroups_Option(getData("Applicable groups"))))
        {
            error = "Failed to wait for Applicable groups option: " + getData("Applicable groups");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ApplicableGroups_Option(getData("Applicable groups"))))
        {
            error = "Failed to click the Applicable groups option: " + getData("Applicable groups");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Applicable groups option: " + getData("Applicable groups"));

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        pause(8000);
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Inspection_Scheduler_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Inspection_Scheduler_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
