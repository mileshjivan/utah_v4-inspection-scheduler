/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Inspection Scheduler Reccurence - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Inspection_Scheduler_Recurrence_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Inspection_Scheduler_Recurrence_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Inspection_Scheduler_Recurrence())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Capture_Inspection_Scheduler_Recurrence()
    {

        //Inspection Scheduler Recurrence
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_Panel()))
        {
            error = "Failed to wait for 'Inspection Scheduler Recurrence' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_Panel()))
        {
            error = "Failed to click on 'Inspection Scheduler Recurrence' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Inspection Scheduler Recurrence' panel.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Recurrence_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Recurrence_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Start date
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.StartDate()))
        {
            error = "Failed to wait for Start date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.StartDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");

        switch (getData("Recurrence frequency"))
        {
            case "Once off":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                break;

            case "Daily":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Recurrence frequency checkbox
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_CheckBox()))
                {
                    error = "Failed to wait for 'Recurrence frequency' checkbox.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_CheckBox()))
                {
                    error = "Failed to click on 'Recurrence frequency' checkbox.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully click 'Recurrence frequency' checkbox.");

                //Number of days to complete
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.NumberOfDays()))
                {
                    error = "Failed to wait for Number of days to complete field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.NumberOfDays(), getData("Number of days to complete")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Number of days to complete") + "'.");

                //End date
                String enddate=SeleniumDriverInstance.getADate("", 5);

                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), enddate))
                {
                    error = "Failed to enter Date Conducted";
                    return false;
                }
//                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EndDate()))
//                {
//                    error = "Failed to wait for End date field.";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), endDate))
//                {
//                    error = "Failed to enter '" + endDate + "'.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");

                break;
            case "Weekly":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Number of days to complete
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.NumberOfDays()))
                {
                    error = "Failed to wait for Number of days to complete field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.NumberOfDays(), getData("Number of days to complete")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Number of days to complete") + "'.");

                //End date
                String enddatew=SeleniumDriverInstance.getADate("", 50);

                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), enddatew))
                {
                    error = "Failed to enter Date Conducted";
                    return false;
                }
                
//                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EndDate()))
//                {
//                    error = "Failed to wait for End date field.";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), endDate))
//                {
//                    error = "Failed to enter '" + endDate + "'.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                break;
            case "Monthly":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Number of days to complete
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.NumberOfDays()))
                {
                    error = "Failed to wait for Number of days to complete field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.NumberOfDays(), getData("Number of days to complete")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Number of days to complete") + "'.");

                //End date
                String enddatem=SeleniumDriverInstance.getADate("", 55);

                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), enddatem))
                {
                    error = "Failed to enter Date Conducted";
                    return false;
                }
//                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EndDate()))
//                {
//                    error = "Failed to wait for End date field.";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), endDate))
//                {
//                    error = "Failed to enter '" + endDate + "'.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                break;
            case "Annually":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Number of days to complete
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.NumberOfDays()))
                {
                    error = "Failed to wait for Number of days to complete field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.NumberOfDays(), getData("Number of days to complete")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Number of days to complete") + "'.");

                //End date
                String enddatea=SeleniumDriverInstance.getADate("", 500);

                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), enddatea))
                {
                    error = "Failed to enter Date Conducted";
                    return false;
                }
//                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EndDate()))
//                {
//                    error = "Failed to wait for End date field.";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), endDate))
//                {
//                    error = "Failed to enter '" + endDate + "'.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                
                //Annually from 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to wait for Annually from dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to click the Annually from dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Annually from dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Annually from"))))
                {
                    error = "Failed to wait for Annually from option: " + getData("Annually from");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Annually from"))))
                {
                    error = "Failed to click the Annually from option: " + getData("Annually from");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Annually from option: " + getData("Annually from"));
                break;
            case "Quarterly":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Number of days to complete
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.NumberOfDays()))
                {
                    error = "Failed to wait for Number of days to complete field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.NumberOfDays(), getData("Number of days to complete")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Number of days to complete") + "'.");

                //End date
                String enddateq=SeleniumDriverInstance.getADate("", 200);

                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), enddateq))
                {
                    error = "Failed to enter Date Conducted";
                    return false;
                }
//                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EndDate()))
//                {
//                    error = "Failed to wait for End date field.";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), endDate))
//                {
//                    error = "Failed to enter '" + endDate + "'.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                
                //Quarter start from 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to wait for Quarter start from dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to click the Quarter start from dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Quarter start from dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Quarter start from"))))
                {
                    error = "Failed to wait for Quarter start from option: " + getData("Quarter start from");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Quarter start from"))))
                {
                    error = "Failed to click the Quarter start from option: " + getData("Quarter start from");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Quarter start from option: " + getData("Quarter start from"));
                break;
            case "Bi-Annually":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Number of days to complete
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.NumberOfDays()))
                {
                    error = "Failed to wait for Number of days to complete field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.NumberOfDays(), getData("Number of days to complete")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Number of days to complete") + "'.");

                //End date
                String enddateba=SeleniumDriverInstance.getADate("", 300);

                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), enddateba))
                {
                    error = "Failed to enter Date Conducted";
                    return false;
                }
                
//                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.EndDate()))
//                {
//                    error = "Failed to wait for End date field.";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.EndDate(), endDate))
//                {
//                    error = "Failed to enter '" + endDate + "'.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                
                //Bi-Annual from 
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to wait for Bi-Annual from dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to click the Bi-Annual from dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Quarter start from dropdown.");

                //Bi-Annual from select
                if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Bi-Annual from"))))
                {
                    error = "Failed to wait for Bi-Annual from option: " + getData("Bi-Annual from");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Option(getData("Bi-Annual from"))))
                {
                    error = "Failed to click the Bi-Annual from option: " + getData("Bi-Annual from");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Bi-Annual from option: " + getData("Bi-Annual from"));
                break;
            default:
                System.out.println(" Sorry we don't have any other options");
        }
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        pause(15000);
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Inspection_Scheduler_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Inspection_Scheduler_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
